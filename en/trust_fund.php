<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <title>Trust Fund</title>
</head>
<body>

<header>
    <div class="container-fluid">
        <div class="row">
            <div class="header">
                <img src="images/header.jpg" alt="">
            </div>
        </div>
    </div>
</header>

<div class="trust_banner">
    <div class="trust_title">
        <h2>The revolving loan program and trust fund</h2>
    </div>

</div>

<div class="container">
    <div class="row">
        <div class="col">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">loans</li>
                </ol>
            </nav>
        </div>
    </div>
</div>

<div class="container ">
    <div class="row ">
        <div class="col-lg-12">
            <div class="trust_loan_tabs ">
                <nav>
                    <div class="d-flex nav" id="nav-tab" role="tablist">
                        <a class="trust_loan_tab active" id="trust-fund-tab" data-toggle="tab" href="#trust-fund" role="tab" aria-controls="trust-fund" aria-selected="true">
                            <div class="dotted_border_green">
                                <div class="trust_loan_tab_inner">
                                    <span class="icon-rev_loan1"></span>
                                    <h3>The revolving loan program</h3>
                                </div>
                            </div>
                        </a>
                        <a class="trust_loan_tab" id="revolving-loan-tab" data-toggle="tab" href="#revolving-loan" role="tab" aria-controls="revolving-loan" aria-selected="false">
                            <div class="dotted_border_green">
                                <div class="trust_loan_tab_inner">
                                    <span class="icon-rev_loan2"></span>
                                    <h3>Trust fund</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>

<section class="trust_loan_tabs_content">
    <div class="trust_loan_tabs_content tab-content" id="nav-tabContent">

        <!-- ========================================================================= -->
        <!-- ============================ TAB ONE CONTENT ============================ -->
        <!-- ========================================================================= -->

        <div class="tab-pane fade show active" id="trust-fund" role="tabpanel" aria-labelledby="trust-fund-tab">
            <div class="container">
                <div class="row jusify-content-center text-center">
                    <div class="col">
                        <div class="trust_loan_sec_one w_90_per">
                            <div class="main_heading">
                                <h1>Revolving Loans Program for finance small and medium farmers and producers in Arab countries</h1>
                            </div>
                            <p>In 2006, AAAID established the Trust Fund, targeting small and medium farmers, producers, and rural women in Sudan. Provided an integrated package of financing services, forming a base for adopting the idea of a revolving loans program for financing small and medium farmers, and producers in Sudan.</p>
                            <p>AAAID has initiated a program of revolving loans for small and medium farmers, and producers in Sudan, supporting this sector by providing timely financing, modern technologies, agricultural extension, agricultural insurance and facilitating access to the market. Aiming to increase agricultural production and improve productivity.</p>
                            <p>AAAID has expanded the activity of the revolving loans program to targeting small and medium farmers, and producers in other Arab countries.</p>

                            <a class="learn_farming" href="">Learn about the small and medium farmer financing service</a>
                        </div>
                    </div>
                </div>
            </div>
            <section class="loan_objectives text-center">
                <div class="container">
                    <div class="row">
                        <div class="col w_90_per">
                            <div class="main_heading">
                                <h1>Objectives of revolving loans program:</h1>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center ">
                        <div class="col-lg-3 col-md-4 col-sm-6 loan_obj_col obj_icon">
                            <div class="loan_obj_icon_box">
                                <div class="loan_obj_icon">
                                    <span class="icon-rev_loan3"></span>
                                </div>
                            </div>
                            <p>The revolving loans is granted to the farmers and producers against affordable guarantee matching their financial ability, the loans are granted to small and medium farmers, livestock breeders, fishers and workers in the field of agricultural industries in rural areas, to order  enhance food security in Arab countries.</p>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 loan_obj_col obj_icon">
                            <div class="loan_obj_icon_box">
                                <div class="loan_obj_icon">
                                    <span class="icon-rev_loan4"></span>
                                </div>
                            </div>
                            <p>Using integrated technical packages, modern mechanization and smart agricultural solutions in the field of agricultural and food production to increase production and productivity.</p>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 loan_obj_col obj_icon">
                            <div class="loan_obj_icon_box">
                                <div class="loan_obj_icon">
                                    <span class="icon-rev_loan5"></span>
                                </div>
                            </div>
                          <p>Achieving an adequate financial return to maintain the sustainability of the program.</p>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 loan_obj_col obj_icon">
                            <div class="loan_obj_icon_box">
                                <div class="loan_obj_icon">
                                    <span class="icon-rev_loan6"></span>
                                </div>
                            </div>
                          <P>The program provides job opportunities for both genders improving their skill, and experience which minimize migration from rural areas to the urban.</P>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 loan_obj_col obj_icon">
                            <div class="loan_obj_icon_box">
                                <div class="loan_obj_icon">
                                    <span class="icon-rev_loan7"></span>
                                </div>
                            </div>
                            <p>Optimum utilization of natural agricultural resources such as lands, water, animal and fish wealth available in Arab countries.</p>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 loan_obj_col obj_icon">
                            <div class="loan_obj_icon_box">
                                <div class="loan_obj_icon">
                                    <span class="icon-rev_loan8"></span>
                                </div>
                            </div>
                          <p>Contributing with financing institutions to build strong partnership with local communities and activate role of community participation to improve production and productivity.</p>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 loan_obj_col obj_icon">
                            <div class="loan_obj_icon_box">
                                <div class="loan_obj_icon">
                                    <span class="icon-rev_loan9"></span>
                                </div>
                            </div>
                          <p>Encouraging the organization of small farmers to enter into professional groups that help in providing services to them.</p>
                        </div>
                    </div>
                </div>
            </section>
            <section class="development_programs">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="main_heading">
                                <h1>The development programs implemented from the revolving loans program during the period 2013-2019</h1>
                            </div>
                        </div>
                    </div>
                    <div class="row dev_charts">
                        <div class="col-lg-6">
                            <div class="countries_pie_chart">
                                <figure class="highcharts-figure">
                                  <div id="countries_pie_chart"></div>
                                </figure>
                            </div>

                        </div>
                        <div class="col-lg-6">
                            <div class="beneficiaries_chart">
                                <figure class="highcharts-figure">
                                  <div id="Beneficiaries_chart"></div>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

        <!-- ========================================================================= -->
        <!-- ============================ TAB TWO CONTENT ============================ -->
        <!-- ========================================================================= -->

        <div class="tab-pane fade" id="revolving-loan" role="tabpanel" aria-labelledby="revolving-loan-tab">
            <div class="container">
                <div class="row jusify-content-center text-center">
                    <div class="col">
                        <div class="trust_loan_sec_one w_90_per">
                            <div class="main_heading">
                                <h1>Trust fund</h1>
                            </div> 
                            <p>The fund was established in 2005 as an initiative by AAAID to alleviate poverty among the vulnerable segments in order to enhance its development role among rural agricultural communities, by mobilizing resources and directing them to contribute to increasing the productivity of the rural agricultural community while supporting basic services to rural areas.</p>
                        </div>
                    </div>
                </div>
            </div>
            <section class="loan_objectives text-center">
                <div class="container">
                    <div class="row">
                        <div class="col w_90_per">
                            <div class="main_heading">
                                <h1>Why Trust Fund?</h1>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center ">
                        <div class="col-lg-3 col-md-4 col-sm-6 loan_obj_col obj_icon">
                            <div class="loan_obj_icon_box">
                                <div class="loan_obj_icon">
                                    <span class="icon-trust_fund1"></span>
                                </div>
                            </div>
                           <p>o provide financing to the poor, especially women, small producers and the subsistence, such as revolving financing for productive activity.</p>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 loan_obj_col obj_icon">
                            <div class="loan_obj_icon_box">
                                <div class="loan_obj_icon">
                                    <span class="icon-trust_fund2"></span>
                                </div>
                            </div>
                         <p>To provide assistance services to poor small farmers, in order to improve productivity, by introducing appropriate technologies and providing plant and animal production inputs.</p>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 loan_obj_col obj_icon">
                            <div class="loan_obj_icon_box">
                                <div class="loan_obj_icon">
                                    <span class="icon-trust_fund3"></span>
                                </div>
                            </div>
                          <p>To provide support for social services in terms of health, education and drinking water in particular.</p>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 loan_obj_col obj_icon">
                            <div class="loan_obj_icon_box">
                                <div class="loan_obj_icon">
                                    <span class="icon-trust_fund4"></span>
                                </div>
                            </div>
                          <p>Capacity building and training for the small and medium business sector.</p>
                        </div>
                    </div>
                </div>
            </section>

            <section class="fund_program ">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="main_heading">
                                <h1>Implemented Fund programs</h1>
                            </div>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-xl-4 col-lg-6">
                            <div class="trust_loan_tabs">
                                <a class="trust_loan_tab fund_prgams" href="#">
                                    <div class="dotted_border_green">
                                        <div class="trust_loan_tab_inner">
                                            <p>Small revolving loans to finance small farmers and producers, especially rural women.</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6">
                            <div class="trust_loan_tabs">
                                <a class="trust_loan_tab fund_prgams" href="#">
                                    <div class="dotted_border_green">
                                        <div class="trust_loan_tab_inner">
                                            <p>Pension support programs (retirees) due to the privacy of this segment and its limited income.</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6">
                            <div class="trust_loan_tabs">
                                <a class="trust_loan_tab fund_prgams" href="#">
                                    <div class="dotted_border_green">
                                        <div class="trust_loan_tab_inner">
                                            <p>A program to support agricultural production in rain-fed areas, by providing financing to farmers' groups, such as financing seeds and agricultural operations, including the spread of zero-tillage technology.</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6">
                            <div class="trust_loan_tabs">
                                <a class="trust_loan_tab fund_prgams" href="#">
                                    <div class="dotted_border_green">
                                        <div class="trust_loan_tab_inner">
                                            <p>Program to support basic services and public utilities (education, health, and water).</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6">
                            <div class="trust_loan_tabs">
                                <a class="trust_loan_tab fund_prgams" href="#">
                                    <div class="dotted_border_green">
                                        <div class="trust_loan_tab_inner">
                                            <p>The fund’s programs diversified to include: crop production, animal husbandry, simple trade and workshops for craftsmen, in addition to training programs, technology transfer and capacity building for small producers.</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6">
                            <div class="trust_loan_tabs">
                                <a class="trust_loan_tab fund_prgams" href="#">
                                    <div class="dotted_border_green">
                                        <div class="trust_loan_tab_inner">
                                            <p>The fund’s activities covered many states of Sudan, and included (30) projects. </p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="funding_sources_banks">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-4 col-lg-6">
                            <div class="funding_sources">
                                <div class="fund_src_icon">
                                    <span class="icon-trust_fund5"></span>
                                </div>
                                <div class="fund_src_text">
                                    <h2>Funding sources</h2>
                                    <p>The fund depends on the contribution of the Authority and other shareholders (the Ministry of Finance, the Savings and Social Development Bank, the National Social Insurance Fund and the Kenana Sugar Company), in addition to donations from other institutions.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-8 col-lg-6">
                            <div class="cooperation_banks">
                                <h2>Cooperation with institutions and banks</h2>
                                <p>In order to preserve the resources of the fund and the good management of loans and guarantees, the fund works on the participation of financial and supervisory institutions and the civil administrations (mayors, sheikhs and symbolic leaders), provided that this participation is a guarantee tool to recover the amounts of financing in addition to increasing the resources of the fund. From:</p>
                                <ul>
                                    <li>The Savings and Social Development Bank in June 2007.</li>
                                    <li>Union of Social Insurance Pension in Sudan in January 2013. Signing an agreement with Al-Suqia Charitable Organization to manage AAAID’s drilling unit.</li>
                                    <li>Signing an agreement with Maarif Charitable Organization to construct and implement concrete ground tanks to provide drinking water for humans and animals in areas of water scarcity.</li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </section>

            <section class="fund_projects">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="main_heading">
                                <h1>Fund projects in the states of Sudan</h1>
                            </div>
                            <div class="funds_text">
                                <div class="funds_inner">
                                    <h2>The fund works in various states of Sudan in the following activities</h2>
                                   <p>Supporting poor families, Pension support, Financing rural women in cooperation with the Rural Women Development Fund, Financing small farmers in the field of plant and animal production, The Trust Fund operates in parallel with the revolving loan program.</p>
                                </div>
                            </div>
                            <div class="search_fields">
                                <form action="">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-6">
                                           <div class="select_box">
                                            <select name="states" id="states"></select>
                                           </div>
                                        </div>
                                        <div class="col-lg-5 col-md-6">
                                           <div class="select_box">
                                            <select name="regions" id="regions"></select>
                                           </div>
                                        </div>
                                        <div class="col-lg-2 col-md-12">
                                            <div class="results_btn">
                                                <button id="search_data">Results</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="total_stats" id="ttl_stats">
                                <div class="statistics">
                                    <div class="stats_inner">
                                        <div class="total">
                                            <h2>Total</h2>
                                        </div>
                                        <div class="vert_border"></div>
                                        <div class="figures_with_icon">
                                            <div class="icon">
                                                <span class="icon-trust_fund7"></span>
                                            </div>
                                            <div class="text">
                                                <strong>Number of households</strong>
                                                <div class="number">
                                                    4,548
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vert_border"></div>
                                        <div class="figures_with_icon">
                                            <div class="icon">
                                                <span class="icon-trust_fund8"></span>
                                            </div>
                                            <div class="text">
                                                <strong>Dependents</strong>
                                                <div class="number">
                                                    4,548
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="search_stats" id="search_stats">
                                <div class="statistics">
                                    <div class="stats_inner">
                                        <div class="figures_with_icon">
                                            <div class="icon">
                                                <span class="icon-trust_fund6"></span>
                                            </div>
                                            <div class="text">
                                                <strong>Type of activity</strong>
                                                <div class="col_one_text">
                                                    Pension support
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vert_border"></div>
                                        <div class="figures_with_icon">
                                            <div class="icon">
                                                <span class="icon-trust_fund7"></span>
                                            </div>
                                            <div class="text">
                                                <strong>Number of households</strong>
                                                <div class="number">
                                                    37
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vert_border"></div>
                                        <div class="figures_with_icon">
                                            <div class="icon">
                                                <span class="icon-trust_fund8"></span>
                                            </div>
                                            <div class="text">
                                                <strong>Dependents</strong>
                                                <div class="number">
                                                    182
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="utility_sites">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-5 col-lg-5">
                            <div class="utility_sites_info">
                                <div class="title">
                                    <h2>Public utility sites Supported according to the activities</h2>
                                </div>
                                <div class="sites_info_box">
                                    <h2 class="state">الجزيرة</h2>
                                    <p class="region_name">الدناقله</p>
                                    <p class="facility">تأهيل مدرسة (تعليم)</p>
                                    <p class="beneficiaries">المجتمع المحلى/ خدمة مجتمعيه</p>

                                    <p class="region_name">ابوحراز</p>
                                    <p class="facility">تأهيل مدرسة (تعليم)</p>
                                    <p class="beneficiaries">المجتمع المحلى/ خدمة مجتمعيه</p>
                                </div>
                                <!-- <div class="subheader">Click countries to view history</div> -->
                                <!-- <div id="country-chart"></div> -->
                            </div>
                        </div>
                        <div class="col-xl-7 col-lg-7">
                            <div class="sudan_map">
                                <div id="sudan_map"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="states_info">
                                <ul>
                                    <li><span class="icon-trust_fund9"></span>Region</i>
                                    <li><span class="icon-trust_fund10"></span>Attachment type</li>
                                    <li><span class="icon-trust_fund11"></span>Beneficiaries Local community Community service</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>
    </div>
</section>

<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="footer">
                <img src="images/footer.jpg" alt="">
            </div>
        </div>
    </div>
</footer>


<!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script> -->
<script
  src="https://code.jquery.com/jquery-3.6.0.js"
  integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
  crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
<!-- <script src="https://code.highcharts.com/highcharts.src.js"></script> -->
<script src="https://code.highcharts.com/maps/highmaps.js"></script>
<script src="https://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="https://code.highcharts.com/mapdata/countries/sd/custom/sd-all-disputed.js"></script>
<script src="js/external.js"></script>
</body>
</html>