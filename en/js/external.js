
// =================================================== //
// ============ BENEFICIARIES BAR CHART ============== //
// =================================================== //

const chart = Highcharts.chart('Beneficiaries_chart', {
    chart: {
        height: 300,
        style: {
            fontFamily: 'din_medium',
            paddingLeft:'0px',
            paddingTop:'10px',
            paddingBottom:'20px',
            paddingRight:'0px',
            borderRadius:'10px'
        }
    },
    title: {
        text: 'Total number of beneficiaries'
    },
    subtitle: {

    },
    xAxis: {
        categories: ['2013', '2014', '2015', '2016', '2017', '2018', '2019'],
        labels: {
            style: {
                fontWeight: 'bold',
            }
        },
    },
    yAxis: {
        labels: {
            style: {
                fontWeight: 'bold',
            }
        },
        title: {
            text: ''
        },
        style:{
            fontWeight:'bold'
        }
    },
    plotOptions: {
        series: {
            pointWidth: 25
        }
    },
    tooltip: {
        formatter: function() {
            return '<strong>Beneficiaries: </strong>'+ this.y;
        }
    },
    credits: {
        enabled: false
    },
    navigation: {
        buttonOptions: {
            enabled: false
        }
    },
    series: [{
        type: 'column',
        data: [1320, 6078, 8180, 18184, 23000, 35885, 58557],
        dataLabels: {
            enabled: true,
            rotation: 0,
            color: 'green',
            align: 'right',
            y: -5,
            x:10,
            style: {
                fontSize: '13px',
                fontFamily: 'helvetica, arial, sans-serif',
                textShadow: false,
                fontWeight: 'normal'

            }
        },
        showInLegend: false,
        color:'#558F42'
    }]
});

// =================================================== //
// ============== COUNTRIES PIE CHART ================ //
// =================================================== //

Highcharts.chart('countries_pie_chart', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        height: 300,
        type: 'pie',
        style: {
            fontFamily: 'din_medium',
        }
    },
    title: {
        text: ''
    },
    tooltip: {
        backgroundColor: '#fff',
        borderColor: 'black',
        borderRadius: 5,
        borderWidth: 0,
        outside:true,
        useHTML:true,
        headerFormat:'',
        pointFormat: '{point.country}<br>{point.slogan}<br>{point.subHeading}<br><br>{point.yearsStats}',
        style: {
            width: 500,
            textAlign: 'right',
            fontSize:'14px',
            padding:'15px'
        }
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    credits: {
        enabled: false
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: true
        }
    },
    legend: {
        align: 'right',
        layout: 'vertical',
        verticalAlign: 'top',
        // rtl: true,
        x: -10,
        y: 80,
        padding: 5,
        itemMarginTop: 5,
        itemMarginBottom: 5,
        itemStyle: {
            lineHeight: '14px',
            fontSize:13,
        }
    },
    navigation: {
        buttonOptions: {
            enabled: false
        }
    },
    series: [{
        name: '',
        colorByPoint: true,
        data: [{
            name: 'Republic of Sudan',
            country: '<span style="width:200px; color:#558F42; font-size:18px; margin-bottom:8px; display:inline-block;">'+
                        'Republic of Sudan'+
                    '</span>',
            slogan: 'برنامج صغار المزارعين والمنتجين',
            subHeading: '<strong style=" margin-top:6px; display:inline-block;">'+
                            'السنة وعدد المستفيدين  '+
                        '</strong>',
            yearsStats: '13060 &nbsp;&nbsp;&nbsp; <strong>2013</strong><br>'+
                        '13060 &nbsp;&nbsp;&nbsp; <strong>2013</strong><br>'+
                        '13060 &nbsp;&nbsp;&nbsp; <strong>2013</strong><br>'+
                        '13060 &nbsp;&nbsp;&nbsp; <strong>2013</strong><br>'+
                        '13060 &nbsp;&nbsp;&nbsp; <strong>2013</strong><br>'+
                        '13060 &nbsp;&nbsp;&nbsp; <strong>2013</strong><br>'+
                        '13060 &nbsp;&nbsp;&nbsp; <strong>2013</strong><br>',
            y: 1,
            sliced: false,
            selected: true,
            color:"#558F42"
        }, {
            name: 'Islamic Republic of Mauritania',
            country: '<span style="color:#558F42; font-size:18px; margin-bottom:8px; display:inline-block;">Islamic Republic of Mauritania</span>',
            slogan: 'برنامج صغار المزارعين والمنتجين',
            subHeading: 'السنة وعدد المستفيدين',
            yearsStats: '13060 &nbsp;&nbsp;&nbsp; <strong>2013</strong><br>13060 &nbsp;&nbsp;&nbsp; 2013<br>13060 &nbsp;&nbsp;&nbsp; 2013',
            y: 1,
            sliced: false,
            selected: true,
            color:"#577483"
        }, {
            name: 'United Republic of Comoros',
            country: '<span style="color:#558F42; font-size:18px; margin-bottom:8px; display:inline-block;">United Republic of Comoros</span>',
            slogan: 'برنامج صغار المزارعين والمنتجين',
            subHeading: 'السنة وعدد المستفيدين',
            yearsStats: '13060 &nbsp;&nbsp;&nbsp; 2013<br>13060 &nbsp;&nbsp;&nbsp; 2013<br>13060 &nbsp;&nbsp;&nbsp; 2013',
            y: 1,
            sliced: false,
            selected: true,
            color:"#ADC6B9"
        }, {
            name: 'Hashemite Kingdom of Jordan',
            country: '<span style="color:#558F42; font-size:18px; margin-bottom:8px; display:inline-block;">Hashemite Kingdom of Jordan</span>',
            slogan: 'برنامج صغار المزارعين والمنتجين',
            subHeading: 'السنة وعدد المستفيدين',
            yearsStats: '13060 &nbsp;&nbsp;&nbsp; 2013<br>13060 &nbsp;&nbsp;&nbsp; 2013<br>13060 &nbsp;&nbsp;&nbsp; 2013',
            y: 1,
            sliced: false,
            selected: true,
            color:"#937963"
        }]
    }]
});

$(function(){
    var states = [
        {
            id:'1',
            name:'state One',
            value:'1'
        },
        {
            id:'2',
            name:'state Two',
            value:'2'
        },
        {
            id:'3',
            name:'state Three',
            value:'3'
        }
    ];
    var regions = [
        {
            id:'1',
            name:'Region One',
            value:'1'
        },
        {
            id:'2',
            name:'Region One adsf',
            value:'1'
        },
        {
            id:'3',
            name:'Reg 2',
            value:'2'
        },
        {
            id:'4',
            name:'Reg 2 adsf',
            value:'2'
        },
        {
            id:'5',
            name:'Reg 3 adsf',
            value:'3'
        }
    ];

    var dataArr = [
        {
            id:'1',
            state: 'state One',
            region: 'Region One',
            activity:'pensoin',
            households:'44',
            dependents:'455',
        },
        {
            id:'2',
            state: 'state One',
            region: 'Region One',
            activity:'pensoin',
            households:'44',
            dependents:'455',
        }
    ];

    $.each(states, function(index, state){
        $('#states').append('<option value="'+ state.value +'" data-searchOpt="'+ state.id +'">' + state.name + '</option>');
    });
    $.each(regions, function(index, reg){
        $('#regions').append('<option value="'+ reg.value +'" data-searchOpt="'+ reg.id +'">' + reg.name + '</option>');
    });
    $("#states").change(function() {
      if ($(this).data('options') === undefined) {
        $(this).data('options', $('#regions option').clone());
      }
      var id = $(this).val();
      var options = $(this).data('options').filter('[value=' + id + ']');
      $('#regions').html(options);
    });

    $('#search_data').on('click',function(e){
        e.preventDefault();
        $('#search_stats').fadeOut('slow');
        $('#ttl_stats').hide('slow');
        $('#search_stats').fadeIn('slow');
        var state_val = $('#states').val();
        var reg_val = $('#regions').val();
        console.log(reg_val +' ========= '+ state_val)
    })
})

 // ================================= SUDAN MAP ============================== //

$(function() {
    var data = [
        {
            "hc-key": "sd-kh",
            "value": 0,
            flag: 'kh',
            selected:false,
            state_name: 'الخرطوم',
            regions: [{
                    region_name: 'الكرياب',
                    facility: 'توصيل كهرباء',
                    beneficiaries: 'المجتمع المحلى/ خدمة مجتمعيه',
                }
            ],
        }, {
            "hc-key": "sd-gz",
            "value": 1,
            flag: 'gz',
            selected:true,
            state_name: 'Al Jazeera',
            regions: [{
                    region_name: 'Eldnqla',
                    facility: 'School rehabilitation (educ.)',
                    beneficiaries: 'Local community / community service',
                },
                {
                    region_name: 'Abo Haraz',
                    facility: 'School rehabilitation (educ.)',
                    beneficiaries: 'Local community / community service',
                }
            ],
        }, {
            "hc-key": "sd-kn",
            "value": 3,
            flag: 'kn',
            selected:false,
            state_name: 'شمال كردفان',
            regions: [{
                    region_name: 'الغبشة',
                    facility: 'توفير مقاعد دراسية (تعليم)',
                    beneficiaries: 'المجتمع المحلى/ خدمة مجتمعيه',
                },
                {
                    region_name: 'ام روابه',
                    facility: 'انشاء حفائر (حصاد مياه)',
                    beneficiaries: 'المجتمع المحلى/ خدمة مجتمعيه',
                }
            ],
        }, {
            "hc-key": "sd-si",
            "value": 4,
            flag: 'si',
            selected:false,
            state_name: 'سنار',
            regions: [{
                    region_name: 'ايد الوليد',
                    facility: 'شبكة مياه (مياه)',
                    beneficiaries: 'المجتمع المحلى/ خدمة مجتمعيه',
                }
            ],
        }, {
            "hc-key": "sd-ks",
            "value": 5,
            flag: 'ks',
            selected:false,
            state_name: 'غرب كردفان',
            regions: [{
                    region_name: 'رية نبلت/ محلية الخوى',
                    facility: 'أنشاء خزان خرصانى أرضى',
                    beneficiaries: 'المجتمع المحلى/خدمة مجتمعية',
                }
            ],
        }, {
            "hc-key": "sd-bn",
            "value": 6,
            flag: 'bn',
            selected:false,
            state_name: 'النيل الازرق',
            regions: [{
                    region_name: 'الدمازين',
                    facility: 'سكن معلمات (تعليم)',
                    beneficiaries: 'المجتمع المحلى/ خدمة مجتمعيه',
                }
            ],
        }
    ],
    mapChart,
    countryChart

    var mapData = Highcharts.geojson(Highcharts.maps['countries/sd/custom/sd-all-disputed']);

    Highcharts.wrap(Highcharts.Point.prototype, 'select', function(proceed) {
        proceed.apply(this, Array.prototype.slice.call(arguments, 1));
        var points = mapChart.getSelectedPoints();
        if (points.length) {
            $('.sites_info_box').html();
            if (points.length === 1) {
                $('.sites_info_box').html('<h2 class="state">'+points[0].state_name+'</h2>');
                for(var i=0; i<points[0].regions.length; i++){
                    $('.sites_info_box').append('<p class="region_name">'+points[0].regions[i].region_name+'</p>');
                    $('.sites_info_box').append('<p class="facility">'+points[0].regions[i].facility+'</p>');
                    $('.sites_info_box').append('<p class="beneficiaries">'+points[0].regions[i].beneficiaries+'</p>');
                }

            }
        }
    });
    mapChart = $('#sudan_map').highcharts('Map', {
        navigation: {
            buttonOptions: {
                enabled: false
            }
        },
        chart:{
            backgroundColor:'transparent',
            height: 450,
        },

        colorAxis: {
            min: 0,
            max: 1000,
            minColor: '#7BB931',
            maxColor: '#7BB931'
        },
        title: {
            text: null
        },
        subtitle: {
            text: null
        },
        credits: {
            enabled: false
        },
        legend: {
            enabled:false
        },
        tooltip:{
            // enabled:false
        },
        series: [{
            data: data,
            mapData: mapData,
            joinBy: 'hc-key',
            allowPointSelect: true,
            cursor: 'pointer',
            states: {
                select: {
                    color: '#124E78',
                }
            }
        }]
    }).highcharts();
});


