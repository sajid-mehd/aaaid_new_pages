<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <title>Trust Fund</title>
</head>
<body dir="rtl">

<header>
    <div class="container-fluid">
        <div class="row">
            <div class="header">
                <img src="images/header.jpg" alt="">
            </div>
        </div>
    </div>
</header>

<div class="trust_banner">
    <div class="trust_title">
        <h2>صندوق الأمانة وبرنامج القروض الدوارة</h2>
    </div>

</div>

<div class="container">
    <div class="row">
        <div class="col">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php">الرئيسية </a></li>
                    <li class="breadcrumb-item active" aria-current="page"> عن الهيئة</li>
                </ol>
            </nav>
        </div>
    </div>
</div>

<div class="container ">
    <div class="row ">
        <div class="col-lg-12">
            <div class="trust_loan_tabs ">
                <nav>
                    <div class="d-flex nav" id="nav-tab" role="tablist">
                        <a class="trust_loan_tab active" id="trust-fund-tab" data-toggle="tab" href="#trust-fund" role="tab" aria-controls="trust-fund" aria-selected="true">
                            <div class="dotted_border_green">
                                <div class="trust_loan_tab_inner">
                                    <span class="icon-rev_loan1"></span>
                                    <h3>برنامج القروض الدوارة</h3>
                                </div>
                            </div>
                        </a>
                        <a class="trust_loan_tab" id="revolving-loan-tab" data-toggle="tab" href="#revolving-loan" role="tab" aria-controls="revolving-loan" aria-selected="false">
                            <div class="dotted_border_green">
                                <div class="trust_loan_tab_inner">
                                    <span class="icon-rev_loan2"></span>
                                    <h3>صندوق الأمانة</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>

<section class="trust_loan_tabs_content">
    <div class="trust_loan_tabs_content tab-content" id="nav-tabContent">

        <!-- ========================================================================= -->
        <!-- ============================ TAB ONE CONTENT ============================ -->
        <!-- ========================================================================= -->

        <div class="tab-pane fade show active" id="trust-fund" role="tabpanel" aria-labelledby="trust-fund-tab">
            <div class="container">
                <div class="row jusify-content-center text-center">
                    <div class="col">
                        <div class="trust_loan_sec_one w_90_per">
                            <div class="main_heading">
                                <h1>برنامج القروض الدوارة لتمويل صغار ومتوسطي المزارعين والمنتجين في الدول العربية </h1>
                            </div>
                            <p>أسست الهيئة العربية صندوق الأمانة (Trust Fund) عام 2006 الذي استهدف صغار ومتوسطي المزارعين والمنتجين والمرأة الريفية في السودان حيث قدم حزمة متكاملة من الخدمات التمويلية لهم والذي كان نواة لتبني فكرة برنامج قروض دوارة لتمويل صغار ومتوسطي المزارعين والمنتجين بالسودان، حيث قدم الصندوق حزمة متكاملة من الخدمات التمويلية لهم.</p>
                            <p>بادرت الهيئة العربية بتنفيذ برنامج للقروض الدوارة لصغار ومتوسطي المزارعين والمنتجين، مستهدفة بذلك دعم هذا القطاع لزيادة الإنتاج الزراعي والارتقاء بالإنتاجية من خلال توفير التمويل في الوقت المناسب وتوفير التقانات الحديثة والإرشاد الزراعي والتأمين الزراعي وتيسير الوصول إلى السوق.</p>
                            <p>وسعت الهيئة نشاط برنامج القروض الدوارة لكي يستهدف صغار ومتوسطي المزارعين والمنتجين في باقي الدول العربية.</p>

                            <a class="learn_farming" href="">تعرّف على خدمة تمويل صغار ومتوسطي المزارعين</a>
                        </div>
                    </div>
                </div>
            </div>
            <section class="loan_objectives text-center">
                <div class="container">
                    <div class="row">
                        <div class="col w_90_per">
                            <div class="main_heading">
                                <h1>أهداف برنامج القروض الدوارة</h1>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center ">
                        <div class="col-lg-3 col-md-4 col-sm-6 loan_obj_col obj_icon">
                            <div class="loan_obj_icon_box">
                                <div class="loan_obj_icon">
                                    <span class="icon-rev_loan3"></span>
                                </div>
                            </div>
                           <p>منح قروض دوَّارة بضمانات تتلاءم مع القدرات المالية لصغار ومتوسطي المنتجين من مزارعين ومربي الماشية وصائدي الأسماك والعاملين في مجال الصناعات الزراعية التحويلية الخاصة في المناطق الريفية وذلك لتعزيز الأمن الغذائي في الدول العربية.</p>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 loan_obj_col obj_icon">
                            <div class="loan_obj_icon_box">
                                <div class="loan_obj_icon">
                                    <span class="icon-rev_loan4"></span>
                                </div>
                            </div>
                          <p>استخدام الحزم التقنية المتكاملة والميكنة الحديثة والحلول الزراعية الذكية في مجال الإنتاج الزراعي والغذائي بغرض زيادة الإنتاج والإنتاجية.</p>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 loan_obj_col obj_icon">
                            <div class="loan_obj_icon_box">
                                <div class="loan_obj_icon">
                                    <span class="icon-rev_loan5"></span>
                                </div>
                            </div>
                          <p>تحقيق عائد مالي مناسب للحفاظ على استدامة عمل البرنامج.</p>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 loan_obj_col obj_icon">
                            <div class="loan_obj_icon_box">
                                <div class="loan_obj_icon">
                                    <span class="icon-rev_loan6"></span>
                                </div>
                            </div>
                          <p>الحد من الهجرة من الريف إلى المدن من خلال توفير فرص العمل للشباب من الجنسين والاستفادة من الموارد البشرية التي تتوفر فيها المهارة والخبرة والإرادة للعمل.</p>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 loan_obj_col obj_icon">
                            <div class="loan_obj_icon_box">
                                <div class="loan_obj_icon">
                                    <span class="icon-rev_loan7"></span>
                                </div>
                            </div>
                          <p>الاستغلال الأمثل للموارد الزراعية الطبيعية من أراض ومياه وثروات حيوانية وسمكية متوفرة في الدول العربية.</p>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 loan_obj_col obj_icon">
                            <div class="loan_obj_icon_box">
                                <div class="loan_obj_icon">
                                    <span class="icon-rev_loan8"></span>
                                </div>
                            </div>
                          <p>المساهمة مع مؤسسات التمويل لبناء شراكة قوية مع المجتمعات المحلية وتفعيل دور المشاركة المجتمعية لتحسين الإنتاج والإنتاجية.</p>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 loan_obj_col obj_icon">
                            <div class="loan_obj_icon_box">
                                <div class="loan_obj_icon">
                                    <span class="icon-rev_loan9"></span>
                                </div>
                            </div>
                          <p>التشجيع على تنظيم صغار المزارعين للدخول في تجمعات مهنية تساعد في تقديم الخدمات إليهم.</p>
                        </div>
                    </div>
                </div>
            </section>
            <section class="development_programs">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="main_heading">
                                <h1>البرامج التنموية المنفذة من برنامج القروض الدَّوارة خلال الفترة 2013-2019</h1>
                            </div>
                        </div>
                    </div>
                    <div class="row dev_charts">
                        <div class="col-lg-6">
                            <div class="countries_pie_chart">
                                <figure class="highcharts-figure">
                                  <div id="countries_pie_chart"></div>
                                </figure>
                            </div>

                        </div>
                        <div class="col-lg-6">
                            <div class="beneficiaries_chart">
                                <figure class="highcharts-figure">
                                  <div id="Beneficiaries_chart"></div>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

        <!-- ========================================================================= -->
        <!-- ============================ TAB TWO CONTENT ============================ -->
        <!-- ========================================================================= -->

        <div class="tab-pane fade" id="revolving-loan" role="tabpanel" aria-labelledby="revolving-loan-tab">
            <div class="container">
                <div class="row jusify-content-center text-center">
                    <div class="col">
                        <div class="trust_loan_sec_one w_90_per">
                            <div class="main_heading">
                                <h1>تعرف على صندوق الأمانة</h1>
                            </div>
                            <p>تمَّ إنشاء الصندوق في عام 2005 كمبادرة من الهيئة لتخفيف حدة الفقر وسط الشرائح الضعيفة تعزيزاً لدورها التنموي وسط المجتمعات الريفية الزراعية، وذلك من خلال تعبئة الموارد وتوجيهها للمساهمة في زيادة إنتاجية المجتمع الريفي الزراعي مع دعم الخدمات الأساسية للمناطق الريفية.</p>
                        </div>
                    </div>
                </div>
            </div>
            <section class="loan_objectives text-center">
                <div class="container">
                    <div class="row">
                        <div class="col w_90_per">
                            <div class="main_heading">
                                <h1>لماذا صندوق الأمانة؟</h1>
                            </div>
                            
                        </div>
                    </div>
                    <div class="row justify-content-center ">
                        <div class="col-lg-3 col-md-4 col-sm-6 loan_obj_col obj_icon">
                            <div class="loan_obj_icon_box">
                                <div class="loan_obj_icon">
                                    <span class="icon-trust_fund1"></span>
                                </div>
                            </div>
                           <p>لتوفير التمويل لمحدودي الدخل خاصة النساء وصغار المنتجين والمعاشيين، كالتمويل الدوار للنشاط الإنتاجي.</p>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 loan_obj_col obj_icon">
                            <div class="loan_obj_icon_box">
                                <div class="loan_obj_icon">
                                    <span class="icon-trust_fund2"></span>
                                </div>
                            </div>
                         <p>لتقديم خدمات العون لمحدودي الدخل من صغار المزارعين ارتقاءً بالإنتاجية، وذلك بإدخال التقنيات المناسبة وتوفير مدخلات الإنتاج النباتي والحيواني.</p>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 loan_obj_col obj_icon">
                            <div class="loan_obj_icon_box">
                                <div class="loan_obj_icon">
                                    <span class="icon-trust_fund3"></span>
                                </div>
                            </div>
                          <p>لتوفير الدعم للخدمات الاجتماعية من صحة وتعليم ومياه الشرب بصفة خاصة.</p>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 loan_obj_col obj_icon">
                            <div class="loan_obj_icon_box">
                                <div class="loan_obj_icon">
                                    <span class="icon-trust_fund4"></span>
                                </div>
                            </div>
                          <p>لبناء القدرات والتدريب لقطاع الأعمال الصغيرة والمتوسطة.</p>
                        </div>
                    </div>
                </div>
            </section>

            <section class="fund_program ">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="main_heading">
                                <h1>برامج الصندوق المنفّذة</h1>
                            </div>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-xl-4 col-lg-6">
                            <div class="trust_loan_tabs">
                                <a class="trust_loan_tab" href="#">
                                    <div class="dotted_border_green">
                                        <div class="trust_loan_tab_inner">
                                            <p>القروض الصغيرة الدوَّارة لتمويل صغار المزارعين والمنتجين خاصة النساء بالأرياف.</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6">
                            <div class="trust_loan_tabs">
                                <a class="trust_loan_tab" href="#">
                                    <div class="dotted_border_green">
                                        <div class="trust_loan_tab_inner">
                                            <p>برامج دعم المعاشيين (المتقاعدين) بحكم خصوصية هذه الشريحة ومحدودية دخلها.</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6">
                            <div class="trust_loan_tabs">
                                <a class="trust_loan_tab" href="#">
                                    <div class="dotted_border_green">
                                        <div class="trust_loan_tab_inner">
                                            <p>برنامج دعم الإنتاج الزراعي في مناطق الزراعة المطرية وذلك عن طريق توفير التمويل لتجمُّعات المزارعين كتمويل التقاوي والعمليات الزراعية بما في ذلك نشر تقانة الزراعة بدون حرث.</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6">
                            <div class="trust_loan_tabs">
                                <a class="trust_loan_tab" href="#">
                                    <div class="dotted_border_green">
                                        <div class="trust_loan_tab_inner">
                                            <p>برنامج دعم الخدمات الأساسية والمرافق العامة (تعليم، صحة، مياه).</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6">
                            <div class="trust_loan_tabs">
                                <a class="trust_loan_tab" href="#">
                                    <div class="dotted_border_green">
                                        <div class="trust_loan_tab_inner">
                                            <p>تنوعت برامج الصندوق لتشمل: انتاج المحاصيل، تربية الحيوان، التجارة البسيطة وورش للحرفيين، بالإضافة لبرامج التدريب و نقل التقانة وبناء القدرات لصغار المنتجين.</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6">
                            <div class="trust_loan_tabs">
                                <a class="trust_loan_tab" href="#">
                                    <div class="dotted_border_green">
                                        <div class="trust_loan_tab_inner">
                                            <p>غطى نشاط الصندوق العديد من ولايات السودان، وشمل عدد (30) مشروعاً استفاد من خدماتها نحو (1,705 أسرة) بإجمالي يصل إلى (8,525 فرد).</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="funding_sources_banks">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-4 col-lg-6">
                            <div class="funding_sources">
                                <div class="fund_src_icon">
                                    <span class="icon-trust_fund5"></span>
                                </div>
                                <div class="fund_src_text">
                                    <h2>مصادر التمويل</h2>
                                    <p>يعتمد الصندوق على مساهمة الهيئة والمساهمين الآخرين (وزارة المالية ومصرف الادخار والتنمية الاجتماعية والصندوق القومي للتأمين الاجتماعي وشركة سكر كنانة)، بالإضافة إلى تبرعات من مؤسسات أخرى.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-8 col-lg-6">
                            <div class="cooperation_banks">
                                <h2>التعاون مع المؤسسات والمصارف</h2>
                                <p>للمحافظة على موارد الصندوق وحُسن إدارة القروض والضمانات، يعمل الصندوق على مشاركة المؤسسات المالية والإشرافية الإدارات الأهلية من (عُمد ومشايخ وقيادات رمزية) على أن تكون تلك المشاركة بمثابة أداة ضمان لاسترجاع مبالغ التمويل بالإضافة إلى زيادة موارد الصندوق، وفى هذا الجانب وقع الصندوق اتفاقيتين مع كل من:</p>
                                <ul>
                                    <li>مصرف الادخار والتنمية الاجتماعية في يونيو 2007م.</li>
                                    <li>اتحاد معاشي التأمين الاجتماعي بالسودان في يناير 2013م. توقيع اتفاقية مع منظمة السقيا الخيرية.لادارة وحدة الحفر التابعة للهيئة</li>
                                    <li>توقيع اتفاقية مع منظمة معارف الخيرية.لانشاء وتنفيذ خزانات خرصانية ارضية لتوفير مياه الشرب للإنسان والحيوان فى مناطق شح المياه</li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </section>

            <section class="fund_projects">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="main_heading">
                                <h1>مشروعات الصندوق بولايات السودان</h1>
                            </div>
                            <div class="funds_text">
                                <div class="funds_inner">
                                    <h2>يعمل الصندوق في مختلف ولايات السودان في الأنشطة التالية:</h2>
                                    <p>دعم الأسر الفقيرة, دعم المعاشين , تمويل المرأة الريفية  بالتعاون مع صندوق تنمية المرأة الريفية, تمويل صغار المزارعين في مجال الإنتاج النباتي والحيواني, يعمل صندوق الأمانة بالتوازي مع برنامج القروض الدوارة</p>
                                </div>
                            </div>
                            <div class="search_fields">
                                <form action="">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-6">
                                           <div class="select_box">
                                            <select name="states" id="states"></select>
                                           </div>
                                        </div>
                                        <div class="col-lg-5 col-md-6">
                                           <div class="select_box">
                                            <select name="regions" id="regions"></select>
                                           </div>
                                        </div>
                                        <div class="col-lg-2 col-md-12">
                                            <div class="results_btn">
                                                <button id="search_data">النتيجة</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="total_stats" id="ttl_stats">
                                <div class="statistics">
                                    <div class="stats_inner">
                                        <div class="total">
                                            <h2>المجمــــــــــــوع</h2>
                                        </div>
                                        <div class="vert_border"></div>
                                        <div class="figures_with_icon">
                                            <div class="icon">
                                                <span class="icon-trust_fund7"></span>
                                            </div>
                                            <div class="text">
                                                <strong>عدد الأسر</strong>
                                                <div class="number">
                                                    4,548
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vert_border"></div>
                                        <div class="figures_with_icon">
                                            <div class="icon">
                                                <span class="icon-trust_fund8"></span>
                                            </div>
                                            <div class="text">
                                                <strong>المُعاليين</strong>
                                                <div class="number">
                                                    4,548
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="search_stats" id="search_stats">
                                <div class="statistics">
                                    <div class="stats_inner">
                                        <div class="figures_with_icon">
                                            <div class="icon">
                                                <span class="icon-trust_fund6"></span>
                                            </div>
                                            <div class="text">
                                                <strong>نـــوع النشــاط</strong>
                                                <div class="col_one_text">
                                                    دعم معاشيين
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vert_border"></div>
                                        <div class="figures_with_icon">
                                            <div class="icon">
                                                <span class="icon-trust_fund7"></span>
                                            </div>
                                            <div class="text">
                                                <strong>عدد الأسر</strong>
                                                <div class="number">
                                                    37
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vert_border"></div>
                                        <div class="figures_with_icon">
                                            <div class="icon">
                                                <span class="icon-trust_fund8"></span>
                                            </div>
                                            <div class="text">
                                                <strong>المُعاليين</strong>
                                                <div class="number">
                                                    182
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="utility_sites">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-5 col-lg-5">
                            <div class="utility_sites_info">
                                <div class="title">
                                    <h2>مواقع المرافق العامة <br>
                                    التي تم دعمها وفق النشاطات</h2>
                                </div>
                                <div class="sites_info_box">
                                    <h2 class="state">الجزيرة</h2>
                                    <p class="region_name">الدناقله</p>
                                    <p class="facility">تأهيل مدرسة (تعليم)</p>
                                    <p class="beneficiaries">المجتمع المحلى/ خدمة مجتمعيه</p>

                                    <p class="region_name">ابوحراز</p>
                                    <p class="facility">تأهيل مدرسة (تعليم)</p>
                                    <p class="beneficiaries">المجتمع المحلى/ خدمة مجتمعيه</p>
                                </div>
                                <!-- <div class="subheader">Click countries to view history</div> -->
                                <!-- <div id="country-chart"></div> -->
                            </div>
                        </div>
                        <div class="col-xl-7 col-lg-7">
                            <div class="sudan_map">
                                <div id="sudan_map"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="states_info">
                                <ul>
                                    <li><span class="icon-trust_fund9"></span>المنطقة</li>
                                    <li><span class="icon-trust_fund10"></span>نوع المرفق</li>
                                    <li><span class="icon-trust_fund11"></span>المستفيدين المجتمع المحلي خدمة مجتمعية</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>
    </div>
</section>

<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="footer">
                <img src="images/footer.jpg" alt="">
            </div>
        </div>
    </div>
</footer>


<!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script> -->
<script
  src="https://code.jquery.com/jquery-3.6.0.js"
  integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
  crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
<!-- <script src="https://code.highcharts.com/highcharts.src.js"></script> -->
<script src="https://code.highcharts.com/maps/highmaps.js"></script>
<script src="https://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="https://code.highcharts.com/mapdata/countries/sd/custom/sd-all-disputed.js"></script>
<script src="js/external.js"></script>
</body>
</html>